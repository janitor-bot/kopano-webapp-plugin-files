kopano-webapp-plugin-files (2.1.5+dfsg1-2) unstable; urgency=medium

  [ Roel van Meer ]
  * [c4edfc7] rebuild patch queue from patch-queue branch
    added patch:
    Fix parse errors.patch
    modified patch:
    modify build.xml to get build done with recent openjdk
    The original modify-build patch changed the build to avoid parse errors,
    but it caused the file js/files.js not to be created or installed. This,
    in turn, caused the plugin to not show up in the plugins list in the
    user options pane. The updated version doesn't change the build as
    much. Instead, the new patch fixes the actual parse errors that the
    build complained about.
    (Closes: #940269)

  [ Carsten Schoenert ]
  * [46721f4] d/control: increase Standards-Version to 4.4.1
  * [6a91966] dh: move over to debhelper-compat
  * [ec28c9a] rebuild patch queue from patch-queue branch
    added patch:
    phpfastcache-upgrade-to-4.3.18.patch
    Fixes CVE-2019-16774

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sun, 05 Jan 2020 12:21:51 +0100

kopano-webapp-plugin-files (2.1.5+dfsg1-1) unstable; urgency=medium

  * [d6fe2d7] New upstream version 2.1.5+dfsg1
  * [ba3db20] d/control: increase Standards-Version to 4.3.0
    No further changes needed.

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sat, 09 Feb 2019 15:27:14 +0100

kopano-webapp-plugin-files (2.1.4+dfsg1-1) unstable; urgency=medium

  * [6d518a4] update Maintainers to new contact address
    Updating the Maintainer email address to
    pkg-giraffe-maintainers@alioth-lists.debian.net.
  * [25cbd3b][4935ef2][64e2a27] debian/control: increase Standards-Version
    finally to 4.2.1
    No further changes needed.
  * [1c1ab91] debian/watch: adding a watch file for k-w-plugin-files
    Adding a debian/watch file to keep track of the upstream changes within
    the upstream git tree.
  * [bfa8cbc][c1025c3] New upstream version 2.1.3+dfsg1 and 2.1.4+dfsg1
  * [c7a2566][a3d0981] rebuild patch queue from patch-queue branch
    added patch:
    modify build.xml to get build done with recent openjdk
    modified patch:
    move-folder-es_CA.UTF-8-to-ca_ES.UTF-8.patch
    Update po file against recent source files.
  * [c6b1231] debian/gbp.conf: filter out also phpfastcache examples
  * [ea0621f] debian/copyright: update project information
  * [1a75dc9] bump compat and debhelper to version 11
    Use the recent version of debhelper and update the compat version
    accordingly.

 -- Carsten Schoenert <c.schoenert@t-online.de>  Thu, 30 Aug 2018 20:24:54 +0200

kopano-webapp-plugin-files (2.1.2+dfsg1-1) unstable; urgency=medium

  * [c990580] New upstream version 2.1.2+dfsg1
  * [41ad139] rebuild patch queue from patch-queue branch
  * [238a418] debian/gbp.conf: adjust debian branch to debian/sid
  * [ab46c1b] d/control: adding Rules-Requires-Root to source section

 -- Carsten Schoenert <c.schoenert@t-online.de>  Fri, 16 Mar 2018 10:10:21 +0100

kopano-webapp-plugin-files (2.1.1+dfsg1-1) experimental; urgency=medium

  * [4f57368] New upstream version 2.1.1+dfsg1
  * [b28121d] rebuild patch queue from patch-queue branch
    Just a small typo fix found by lintian.
  * [09ff2db] ebian/control: increase Standards-Version to 4.1.3
    No further changes needed.
  * [b528663] adding and adjust Vcs fields to salsa.debian.org

 -- Carsten Schoenert <c.schoenert@t-online.de>  Thu, 15 Feb 2018 21:56:24 +0100

kopano-webapp-plugin-files (2.1.0+dfsg1-1) experimental; urgency=medium

  * initial packaging (Closes: #877480)

  * [318d017] debian/* basic debianization
  * [a4d2eb3] debian/copyright: update info about PHP composer files
  * [8357c3d] d/README.source: adding some source related information
  * [b52fb07] New upstream version 2.1.0+dfsg1
  * [0f4de28] rebuild patch queue from patch-queue branch
  * [6927743] debian/control: increase Standards-Version to 4.1.2
  * [e8d98aa] remove Lintian override for k-w-p-f

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sat, 09 Dec 2017 17:23:21 +0100
